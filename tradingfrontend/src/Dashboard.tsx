import React from "react";

import InfoPanel from "./panels/InfoPanel";
import AllocationPanel from "./panels/AllocationPanel";
import PositionsPanel from "./panels/PositionsPanel";
import PerformancePanel from "./panels/PerformancePanel";

export default function Dashboard() {
  return (
    <div className="panels">
      <div className="panel-info">
        <InfoPanel />
      </div>
      <div className="panel-allocation">
        <PerformancePanel/>
      </div>
      <div className="panel-allocation">
        <AllocationPanel/>
      </div>
      <div className="panel-positions">
        <PositionsPanel />
      </div>
      


    </div>
  )
}