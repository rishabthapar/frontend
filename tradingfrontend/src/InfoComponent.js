import * as React from 'react'
import * as ReactDOM from 'react-dom'
import InfoService from './InfoService'

import { NumericTextBox } from '@progress/kendo-react-inputs';

class InfoComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentBalance:0,
            addAmount: 0,
            formatOptions: {
                style: 'currency',
                currency: 'USD',
                currencyDisplay: 'name'
            }
        };
    }

    componentDidMount(){
        InfoService.getCashBalance().then((response) => {
            //console.log(response.data[0].cash)
            this.setState({
                currentBalance: response.data[0].cash,
                addAmount:0
            }) 
        });
    }
    


    setAddAmount = (e) => {
        //console.log("add amount: ", typeof(e.value))
        this.setState({
            currentBalance: this.state.currentBalance,
            addAmount: e.value
        });
    }


    render() {
        return (
            <div>
                <label>
                USD
                    <NumericTextBox
                        placeholder="Enter amount of Cash to add"
                        value={this.state.addAmount}
                        onChange={this.setAddAmount}
                    /> 
                </label>
                <button onClick={() => {
                InfoService.updateCashBalance(this.state.addAmount)
                this.setState({ currentBalance: this.state.currentBalance + this.state.addAmount,
                                addAmount: 0 });
                }}>
                    Add Cash
                </button>
                <p>Current Balance: $ {this.state.currentBalance}</p>
            </div>
        );
    }
}

export default InfoComponent