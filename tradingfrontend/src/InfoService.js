import axios from 'axios'

const PORTFOLIO_REST_API_URL = 'http://localhost:8080/portfolio/';
const PORTFOLIO_PATCH_URL = 'http://localhost:8080/portfolio/updateCash/5f627a4c4af42e782002a562'

class InfoService{
    getCashBalance(){
        return axios.get(PORTFOLIO_REST_API_URL)
    }

    updateCashBalance(amount){
        
        var transfer = {cash: amount}
        //console.log("updateCashBalance",amount)
        const response = axios.patch(PORTFOLIO_PATCH_URL, amount, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            console.log(res);
          })
          .catch(err => console.log(err));
        //console.log("Response " + response)
        
    }
}

export default new InfoService()