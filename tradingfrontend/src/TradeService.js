import axios from 'axios'
import qs from 'querystring'
const TRADES_REST_API_URL = 'http://localhost:8090/trades';
const TRADES_POST_URL = 'http://localhost:8090/trades';

const TRADES_BY_TICKER = "http://localhost:8090/trades/ticker/"

class TradeService {

    getTrades(){
        return axios.get(TRADES_REST_API_URL);
    }

    addTrades(ticker, price, quantity, type){
        // var querystring = require('querystring');

        const response = axios.post(TRADES_POST_URL,{
            "ticker": ticker, 
            "price": price, 
            "quantity": quantity, 
            "type": type,
            "status":"CREATED",
        },{
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            console.log(res);
          })
          .catch(err => console.log(err));
    }
}

export default new TradeService();