import React from 'react';
import TradeService from './TradeService';

class TradeComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            trades: []
        }
    }

    componentDidMount() {
        TradeService.getTrades().then((response) => {
            this.setState({ trades: response.data })
        });
        console.log(this.state.trades)
    }

    render() {
        return (
            <div>
                <h1 className="text-center"> Trade List</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date Created</th>
                            <th>Ticker</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Type</th>
                        </tr>

                    </thead>
                    <tbody>
                        {
                            this.state.trades.map(
                                trade =>
                                    <tr key={trade.id}>
                                        <td>{trade.id}</td>
                                        <td>{trade.dateCreated}</td>
                                        <td>{trade.ticker}</td>
                                        <td>{trade.quantity}</td>
                                        <td>{trade.price}</td>
                                        <td>{trade.status}</td>
                                        <td>{trade.type}</td>
                                    </tr>
                            )
                        }

                    </tbody>
                </table>

            </div>

        )
    }
}

export default TradeComponent