import * as React from 'react'
import * as ReactDOM from 'react-dom'
import TradeService from './TradeService'
import InfoService from './InfoService'
import TradeComponent from './TradeComponent'
import { NumericTextBox } from '@progress/kendo-react-inputs';
import InfoComponent from './InfoComponent';
import { isNullOrUndefined } from 'util'

class BuyComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            ticker:null,
            quantity:0,
            price:0,
            total: 0,
            currentBalance:0
        };
    }


    componentDidMount(){
        InfoService.getCashBalance().then((response) => {
            //console.log(response.data[0].cash)
            this.setState({
                currentBalance: response.data[0].cash,
            }) 
        });
    }

    setPrice = (e) => {
        this.setState({
            price: e.value
        });
    }

    setQuantity = (e) => {
        this.setState({
            quantity: e.value
        });
    }

    setTicker = (e) => {
        this.setState({
            ticker: e.target.value
        });
    }

    render() {
        return (
            <div>
            
            <h3>TRADE Stock</h3>
            <label>
                Ticker <input id="ticker" type="text" value={this.state.ticker} onChange={this.setTicker}/>
            </label>
            <label>
                Price  <NumericTextBox
                        placeholder="$0"
                        value={this.state.price}
                        onChange={this.setPrice}
                    />
            </label>
            <label>
                Quantity  <NumericTextBox
                        placeholder="$0"
                        value={this.state.quantity}
                        onChange={this.setQuantity}
                    />
            </label>
            <p>Total: $ {this.state.quantity * this.state.price}</p>
            <div id="button" >
                <button onClick={() => {
                    var that = this;
                    var total = -(this.state.quantity * this.state.price);
                    TradeService.addTrades(this.state.ticker, this.state.price, this.state.quantity, "BUY");
                    InfoService.updateCashBalance(total);
                    //TradeComponent.componentDidMount();
                    //InfoComponent.componentDidMount();
                    this.setState({ 
                        currentBalance: this.state.currentBalance + total,
                        total: 0 ,
                        price: 0,
                        quantity:0,
                        ticker: "",
                        total: total
                    });
                }}>
                    Buy
                </button>
                <button onClick={() => {
                    var total = this.state.quantity * this.state.price;
                    TradeService.addTrades(this.state.ticker, this.state.price, this.state.quantity, "SELL");
                    InfoService.updateCashBalance(total);
                   // TradeComponent.componentDidMount();
                    //InfoComponent.componentDidMount();
                    this.setState({ 
                        currentBalance: this.state.currentBalance + total,
                        total: 0 ,
                        price: 0,
                        quantity:0,
                        ticker:"",
                        total: total
                    });
                }}>
                    Sell
                </button>
            </div>   
            </div>
        );
    }
}


export default BuyComponent