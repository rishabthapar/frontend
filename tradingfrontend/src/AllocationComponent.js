import React from 'react';
import TradeService from './TradeService';
import {
    Chart,
    ChartLegend,
    ChartSeries,
    ChartSeriesItem,
    ChartSeriesLabels,
    ChartTitle,
    ChartTooltip
  } from "@progress/kendo-react-charts";
  const labelContent = (props) => {
    let formatedNumber = Number(props.dataItem.value).toLocaleString(undefined);
    return `${props.dataItem.ticker} : ${formatedNumber}`;
}
  const renderTooltip = (e) => {
      return <div>{e.point ? e.point.category : ""}</div>;
    };

 class AllocationComponent extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            trades: [],
            dataPoints:[]
        }
    }

    componentDidMount() {
        TradeService.getTrades().then((response) => {
            let data = []
            response.data.forEach(trade => {
                console.log(trade.ticker)
                console.log(trade.quantity * trade.price)
                console.log(trade.status)
                if(trade.status === "FILLED"){
                    data.push({"ticker":trade.ticker, "value":((trade.quantity * trade.price))})
                }
                //
               // data.push((trade.quantity * trade.price))
                //data.push(trade.ticker)
            });

            this.setState({ trades: response.data,
                            dataPoints: data
                            })
            console.log(this.state.dataPoints)

            });
        
    }



    render(){
        return(<Chart>
            <ChartTitle text={"Asset Allocation"}></ChartTitle>
            <ChartSeries>
              <ChartSeriesItem type="donut" data={this.state.dataPoints}  >
              <ChartSeriesLabels content={labelContent} background="none" color="#fff"  />
              </ChartSeriesItem>
            </ChartSeries>
            <ChartLegend position="bottom"></ChartLegend>
            <ChartTooltip render={renderTooltip} />
          </Chart>)
    }
}
export default AllocationComponent;