import React from "react";
import {
  Chart,
  ChartLegend,
  ChartSeries,
  ChartSeriesItem,
  ChartSeriesLabels,
  ChartTitle,
  ChartTooltip
} from "@progress/kendo-react-charts";
import AllocationComponent from "./../AllocationComponent"

const labelContent = (e: any) => (`${e.value}%`);

const renderTooltip = (e: any) => {
  return <div>{e.point ? e.point.category : ""}</div>;
};


export default function AllocationPanel() {
  return (<><h2> AllocationPanel </h2>
    
    <div className="AllocationPanel">
      <AllocationComponent/>
    </div></>)
}
